package bearman;
import food.*;
import food.Food;

public class PizzaEatingBearman extends Bearman{
	public void eat(Food food) {
		food.feedBearman(this);
	}
}
