package food;

import bearman.Bearman;
import bearman.PizzaEatingBearman;
import bearman.TacoEatingBearman;

public class Food {

	/**
	 * Simple food satisfies simple bearman
	 * 
	 * @param bearman
	 */
	public void feedBearman(Bearman bearman) {
		bearman.setHungry(false);
	}

	/**
	 * Simple food does not satisfy the hunger of those with more sophisticated
	 * taste
	 * 
	 * @param pizzaEater
	 *            Bearman to eat food
	 */
	public void feedBearman(PizzaEatingBearman pizzaEater) {
		pizzaEater.setHungry(true);
	}

	/**
	 * Simple food does not satisfy the hunger of those with more sophisticated
	 * taste
	 * 
	 * @param tacoEater
	 *            Bearman to eat food
	 */
	public void feedBearman(TacoEatingBearman tacoEater) {
		tacoEater.setHungry(true);
	}

}
